# variable "domains" {
#   description = "The domains for which managed certificate needed. These domains are pointed to the same LB ip"
#   type        = list(string)
#   default     = []
# }


variable "name_suffix" {
  description = "An arbitrary suffix that will be added to the end of the resource name(s). For example: an environment name, a business-case name, a numeric id, etc."
  type        = string
  validation {
    condition     = length(var.name_suffix) <= 14
    error_message = "A max of 14 character(s) are allowed."
  }
}


variable "lb_ip_name" {
  description = "ip name of the load balancer. Leave empty if you wanna create"
  type        = string
  default     = ""
}

variable "serverless_backends" {
  description = "backends of the load balancer"
  type = list(object({
    service_name = string
    service_type = string
    region       = string
    host         = string
    path         = string
    url_rewrite  = string
  }))
  default = []
}

variable "bucket_backends" {
  description = "backends of the load balancer"
  type = list(object({
    bucket_name = string
    host        = string
    path        = string
    url_rewrite = string
  }))
  default = []
}



variable "allow-http" {
  default = false
}
