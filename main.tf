terraform {
  required_version = ">= 0.14.11" # see https://releases.hashicorp.com/terraform/
}
locals {
  serverless_host_names = toset([for k in var.serverless_backends : k.host])
  bucket_host_names     = toset([for k in var.bucket_backends : k.host])
  host_names            = toset(concat(tolist(local.serverless_host_names), tolist(local.bucket_host_names)))
  path_matcher_hashmap  = { for k in local.host_names : k => replace(k, ".", "") }
  serverless_backends   = [for k, v in var.serverless_backends : merge(v, { servicelink = "${google_compute_backend_service.serverless[k].self_link}" })]
  bucket_backends       = [for k, v in var.bucket_backends : merge(v, { servicelink = "${google_compute_backend_bucket.backend_bucket[k].self_link}" })]
  backends              = concat(local.bucket_backends, local.serverless_backends)
  url_map               = { for k in local.backends : local.path_matcher_hashmap[k.host] => k... }
}

resource "google_compute_managed_ssl_certificate" "default" {
  name = "${var.name_suffix}-cert"
  managed {
    domains = tolist(local.host_names)
  }
}

#create static ip for load balancer if ip is not provided 
resource "google_compute_global_address" "lb_ip" {
  count        = var.lb_ip_name != "" ? 0 : 1
  name         = "${var.name_suffix}-ip"
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
  lifecycle {
    prevent_destroy = true
  }
}

//if static ip is provided use that one
data "google_compute_global_address" "my_address" {
  count = var.lb_ip_name != "" ? 1 : 0
  name  = var.lb_ip_name
}

// create 1 neg for 1 serverless service
resource "google_compute_region_network_endpoint_group" "serverless_neg" {
  count                 = length(var.serverless_backends)
  name                  = "neg-${count.index}"
  network_endpoint_type = "SERVERLESS"
  region                = var.serverless_backends[count.index].region
  dynamic "cloud_function" {
    for_each = var.serverless_backends[count.index].service_type == "CLOUD_FUNCTION" ? [1] : []
    content {
      function = var.serverless_backends[count.index].service_name
    }
  }
  dynamic "cloud_run" {
    for_each = var.serverless_backends[count.index].service_type == "CLOUD_RUN" ? [1] : []
    content {
      service = var.serverless_backends[count.index].service_name
    }
  }
}

//create 1 backend service for 1 neg
resource "google_compute_backend_service" "serverless" {
  count = length(var.serverless_backends)
  name  = "serverless-backend-${count.index}"

  protocol    = "HTTP"
  port_name   = "http"
  timeout_sec = 30

  backend {
    group = google_compute_region_network_endpoint_group.serverless_neg[count.index].id
  }
}
// create 1 bucket backend for 1 static bucket
resource "google_compute_backend_bucket" "backend_bucket" {
  count       = length(var.bucket_backends)
  name        = "my-backend-bucket-${count.index}"
  bucket_name = var.bucket_backends[count.index].bucket_name
}


//create 1 url map for 1 lb
resource "google_compute_url_map" "default" {
  count           = length(var.serverless_backends) == 0 ? 0 : 1
  name            = "url-map-${var.name_suffix}"
  default_service = google_compute_backend_service.serverless[0].self_link
  dynamic "host_rule" {
    for_each = local.path_matcher_hashmap
    content {
      hosts        = [host_rule.key]
      path_matcher = host_rule.value
    }
  }

  dynamic "path_matcher" {
    for_each = local.url_map
    content {
      name            = path_matcher.key
      default_service = google_compute_backend_service.serverless[0].self_link

      dynamic "path_rule" {
        for_each = path_matcher.value
        content {
          paths   = [path_rule.value.path]
          service = path_rule.value.servicelink
          dynamic route_action {
            for_each = path_rule.value.url_rewrite != "" ? [1] : []
            content{
            url_rewrite {
              path_prefix_rewrite = path_rule.value.url_rewrite
            }
            }
          }
        }
      }
    }
  }
}


//-----------https start
resource "google_compute_target_https_proxy" "default" {
  name = "${var.name_suffix}-https-proxy"

  url_map = google_compute_url_map.default[0].id
  ssl_certificates = [
    google_compute_managed_ssl_certificate.default.id
  ]
}


resource "google_compute_global_forwarding_rule" "default" {
  name = "${var.name_suffix}-lb"

  target     = google_compute_target_https_proxy.default.id
  port_range = "443"
  ip_address = var.lb_ip_name != "" ? data.google_compute_global_address.my_address[0].address : google_compute_global_address.lb_ip[0].address
}
//-----------https end

//-----------http start
resource "google_compute_target_http_proxy" "http" {
  count   = var.allow-http == true ? 1 : 0
  name    = "${var.name_suffix}-http"
  url_map = google_compute_url_map.default[0].id
}

resource "google_compute_global_forwarding_rule" "http" {
  count = var.allow-http == true ? 1 : 0
  name  = "${var.name_suffix}-lb-http"

  target     = google_compute_target_http_proxy.http[0].id
  port_range = "80"
  ip_address = var.lb_ip_name != "" ? data.google_compute_global_address.my_address[0].address : google_compute_global_address.lb_ip[0].address
}
//-----------http end
